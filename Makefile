etc := $(shell find etc -type f)
elm_sources := $(shell find src -type f -name "*.elm")
elm_mains := $(shell find src -type f -name "*.elm" -d 1)
bld := .bld
gs_bucket := www.iuriikomarov.com

PHONY: build clean format deploy run
build: $(bld)

clean:
	rm -rf $(bld)

format:
	elm-format $(elm_sources) --yes

deploy: clean build
	gsutil cp $(bld)/* gs://$(gs_bucket)
	gsutil acl ch -u AllUsers:R gs://$(gs_bucket)/*

run:
	@ nodemon \
		--watch etc \
		--watch src \
		--ext "html css elm" \
		--exec "make build && http-server $(bld) -p 5000"

$(bld): $(etc:etc/%=$(bld)/%) $(bld)/index.js

$(bld)/index.js: $(elm_sources)
	@ mkdir -p $(@D)
	@ elm make $(elm_mains) --optimize --output $@
	@ mv $@ $@.tmp
	@ uglifyjs $@.tmp \
		--compress 'pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters,keep_fargs=false,unsafe_comps,unsafe' \
	| uglifyjs --mangle --output=$@
	@ echo
	@ echo 'Compiled:' && du -sh $@.tmp
	@ echo 'Minified:' && du -sh $@
	@ rm -rf $@.tmp

$(bld)/%: etc/%
	@ mkdir -p $(@D)
	@ cp $< $(@D)