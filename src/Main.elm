module Main exposing (main)

import Browser
import Browser.Navigation as Nav
import Html as H
import Html.Attributes as HA
import Url


type alias Flags =
    {}


type Msg
    = DoLinkClicked Browser.UrlRequest
    | DoUrlChanged Url.Url


type alias Model =
    { title : String
    , key : Nav.Key
    , initialFlags : Flags
    , page : Page
    }


type Page
    = Index
    | Trap


type alias Link =
    ( String, String )


links : List Link
links =
    [ ( "https://facebook.com/iuriikomarov", "fcbk" )
    , ( "https://github.com/iuriikomarov", "gthb" )
    , ( "https://pinterest.com/iuriikomarov", "pntrst" )
    , ( "https://medium.com/@iuriikomarov", "mdm" )
    , ( "mailto:iuriikomarov@icloud.com", "@" )
    , ( "/trap", "trap" )
    ]


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        , onUrlChange = DoUrlChanged
        , onUrlRequest = DoLinkClicked
        }


init : Flags -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    ( Model "iurii komarov" key flags Index, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        DoLinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        DoUrlChanged url ->
            ( updateUrl url model, Cmd.none )


updateUrl : Url.Url -> Model -> Model
updateUrl url model =
    { model | page = if url.path == "/" then Index else Trap }


view : Model -> Browser.Document Msg
view model =
    Browser.Document model.title <| viewPage model


viewLink : Link -> H.Html Msg
viewLink ( linkHref, linkText ) =
    H.a [ HA.href linkHref ] [ H.text linkText ]


viewPage : Model -> List (H.Html Msg)
viewPage model =
    case model.page of
        Index ->
            [ H.h1 [] <| H.span [] [ H.text "iurii komarov:" ] :: List.map viewLink links ]

        Trap ->
            [ H.h1 [] [ H.text "there's nothing here" ] ]
